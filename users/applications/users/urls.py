from django.contrib import admin
from django.urls import path
from . import views
app_name ="users_app"
urlpatterns = [
    path(
        'register-user/', 
        views.UserRegisterView.as_view(),
        name = 'register_user',
    ),
    path(
        'login/', 
        views.LoginUser.as_view(),
        name = 'user-login',
    ),
    path(
        'logout/', 
        views.LogoutView.as_view(),
        name = 'user-logout',
    ),
    path(
        'update-password/', 
        views.UpdatePasswordView.as_view(),
        name = 'user-update',
    ),
    path(
        'verification/<pk>/', 
        views.UpdatePasswordView.as_view(),
        name = 'user-verification',
    ),

        
]