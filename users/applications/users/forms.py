
from django import forms

from .models import User
from django.contrib.auth import authenticate

class UserRegisterForm(forms.ModelForm):
    """Form definition for UserRegisterForm"""
    password1  = forms.CharField(
        label="password", 
        required=False,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Password'
            }
        )
    )
    password2 = forms.CharField(
        label="password", 
        required=False,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Repeat Password'
            }
        )
    )
    class Meta:
        model= User
        fields = (
            'username',
            'email',
            'first_name',
            'last_name',
            'genrer',
            
        )
    def clean_password2(self):
        if self.cleaned_data['password1'] != self.cleaned_data['password2']:
            self.add_error('password2','No coincide las contraseñas')

class LoginForm(forms.Form):
    username  = forms.CharField(
        label="username", 
        required=False,
        widget=forms.TextInput(
            attrs={
                'placeholder':'username',
                'style':'{ margin:10px}',
                
            }
        )
    )
    
    password  = forms.CharField(
        label="password", 
        required=False,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Password'
            }
        )
    )
    def clean(self):
        cleaned_data = super(LoginForm,self).clean()
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        if not authenticate(username=username,password=password):
            raise forms.ValidationError('Los datos del usuario no son correctos')
        return self.cleaned_data

class UpdatePasswordForm(forms.Form):
    password1  = forms.CharField(
        label="Contraseña", 
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña Actual',
                'style':'{ margin:10px}',
                
            }
        )
    )
    password2  = forms.CharField(
        label="Contraseña", 
        required=True,
        widget=forms.PasswordInput(
            attrs={
                'placeholder':'Contraseña Nueva',
            }
        )
    )

class VerificationForm(forms.Form):
    code_register = forms.CharField(required=True)
    def clean_code_register(self):
        code = self.cleaned_data['code_register']
        if len(code) == 6:
            #verificar si el codigo y el id del usuario son validos
            active = User.objects.code_validate(
                self.kargs['pk'],
                code
            )
            if not active:
                raise forms.ValidationError('Codigo incorrecto')
        else:
            raise forms.ValidationError('Codigo incorrecto')