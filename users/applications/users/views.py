from .functions import code_generator
from django.core.mail import send_mail


from django.shortcuts import render
from django.views import View
from django.views.generic.edit import(
    FormView
)
from django.urls import reverse_lazy,reverse
from django.contrib.auth import authenticate,login,logout
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin

from .forms import (
    UserRegisterForm, 
    LoginForm,
    UpdatePasswordForm,
    VerificationForm
)
from .models import User
# Create your views here.

class UserRegisterView(FormView):
    template_name = 'users/user_register.html'
    form_class = UserRegisterForm
    success_url='/'
    
    def form_valid(self, form):
        #generate code
        codigo = code_generator()
        
        usuario =User.objects.create_user(
            form.cleaned_data['username'],
            form.cleaned_data['email'],
            form.cleaned_data['password1'],
            first_name = form.cleaned_data['first_name'],
            last_name =form.cleaned_data['last_name'],
            genrer = form.cleaned_data['genrer'],
            cod_register = codigo

        )
        # send the code at email
        asunto ='Confirmacion de email'
        msg = 'Codigo de verificacion: ' + codigo
        email = 'andresabyac@gmail.com'
        send_mail(asunto, msg,email, [form.cleaned_data['email'],])
        #redirect validate display
        return HttpResponseRedirect(
            reverse(
                'users_app:user-verification',
                kwargs={
                    'pk':usuario.id
                }
            )
        )
        
    
    
class LoginUser(FormView):
    template_name='users/login.html'
    form_class= LoginForm
    success_url=reverse_lazy('home_app:panel')
    
    def form_valid(self, form):
        #verification with authenticate
        user = authenticate(
            username = form.cleaned_data['username'],
            password =form.cleaned_data['password']
        )
        login(self.request,user)
        return super(LoginUser,self).form_valid(form)

class LogoutView(View):
    def get(self, request, *args,**kgargs):
        logout(request)
        return HttpResponseRedirect(
            reverse(
                'users_app:user-login'
            )
        )
class UpdatePasswordView(LoginRequiredMixin,FormView):
    template_name='users/update_password.html'
    form_class= UpdatePasswordForm
    success_url=reverse_lazy('users_app:user-login')
    login_url = reverse_lazy('users_app:user-login')
    
    def form_valid(self, form):
        #user actual
        usuario = self.request.user
        #authenticate process
        user = authenticate(
            username = usuario.username,
            password =form.cleaned_data['password1']
        )
        if user:
            new_password = form.cleaned_data['password2']
            usuario.set_password(new_password)
            usuario.save()
        logout(self.request)
        return super(UpdatePasswordView,self).form_valid(form)

class VerificationView(FormView):
    template_name='users/verification.html'
    form_class= VerificationForm
    success_url=reverse_lazy('user_app:user-login')
    
    def form_valid(self, form):
        #verification with authenticate
        return super(VerificationView,self).form_valid(form)